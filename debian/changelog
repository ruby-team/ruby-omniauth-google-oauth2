ruby-omniauth-google-oauth2 (1.1.1-2) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.6.2 (no changes needed)
  * drop X?-Ruby-Versions field

 -- Mohammed Bilal <mdbilal@disroot.org>  Fri, 10 Feb 2023 21:09:57 +0530

ruby-omniauth-google-oauth2 (1.1.1-1) experimental; urgency=medium

  * Team upload.
  * New upstream version 1.1.1
  * d/control: add ruby-oauth2 as Build-Dep

 -- Mohammed Bilal <mdbilal@disroot.org>  Sun, 04 Dec 2022 18:21:10 +0000

ruby-omniauth-google-oauth2 (1.0.1-1) experimental; urgency=medium

  * Team Upload.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

  [ Cédric Boutillier ]
  * [ci skip] Update team name
  * [ci skip] Add .gitattributes to keep unwanted files out of the
    source package

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Set upstream metadata fields: Bug-Submit.
  * Update standards version to 4.5.0, no changes needed.
  * Remove constraints unnecessary since buster

  [ Mohammed Bilal ]
  * New upstream version 1.0.1 (Closes: #999716)
  * Tighten dependencies
  * Bump debhelper compatibility level to 13
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Mohammed Bilal <mdbilal@disroot.org>  Tue, 17 May 2022 07:55:22 +0000

ruby-omniauth-google-oauth2 (0.6.0-2) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Pirate Praveen ]
  * Reupload to unstable (gitlab in unstable cannot be supported meaningfully)
  * Bump Standards-Version to 4.4.1 (no changes needed)
  * Drop compat file, rely on debhelper-compat and bump compat level to 12
  * Add myself as an uploader

 -- Pirate Praveen <praveen@debian.org>  Sat, 16 Nov 2019 22:51:29 +0530

ruby-omniauth-google-oauth2 (0.6.0-1) experimental; urgency=medium

  * Team upload
  * New upstream version 0.6.0
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Add Breaks gitlab << 11.7 and tighten ruby-jwt >= 2.0

 -- Pirate Praveen <praveen@debian.org>  Tue, 05 Feb 2019 19:15:52 +0530

ruby-omniauth-google-oauth2 (0.5.3-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.5.3
  * Bump Standards-Version to 4.1.4 (no changes needed)
  * Use salsa.debian.org in Vcs-* fields
  * Run dh-make-ruby -w and update watch, rules and ruby-tests.rake

 -- Pirate Praveen <praveen@debian.org>  Wed, 25 Apr 2018 15:20:20 +0530

ruby-omniauth-google-oauth2 (0.5.2-2) unstable; urgency=medium

  * Team upload.
  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Sun, 18 Mar 2018 21:17:27 +0530

ruby-omniauth-google-oauth2 (0.5.2-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump debhelper compatibility to 11
  * Bump Standards-Version to 4.1.3 (no changes needed)
  * Add patch to disable tests running rubocop

 -- Balasankar C <balasankarc@debian.org>  Tue, 13 Mar 2018 12:57:30 +0530

ruby-omniauth-google-oauth2 (0.4.1-1) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Lucas Kanashiro ]
  * Imported Upstream version 0.4.1
  * debian/rules: export GEM2DEB_TEST_RUNNER = --check-dependencies
  * debian/ruby-tests.rake: update test files regex
  * debian/examples: install examples dir
  * Update years of upstream copyright
  * Update Debian packaging copyright
  * Declare compliance with Debian policy 3.9.8
  * debian/control: update build and runtime dependencies

 -- Lucas Kanashiro <kanashiro@debian.org>  Wed, 22 Jun 2016 10:32:56 -0300

ruby-omniauth-google-oauth2 (0.2.4-1) unstable; urgency=low

  * New upstream release

 -- Nitesh A Jain <niteshjain92@gmail.com>  Sat, 26 Apr 2014 21:51:32 +0530

ruby-omniauth-google-oauth2 (0.2.2-1) unstable; urgency=low

  * Initial release (Closes: #721846)

 -- Nitesh A Jain <niteshjain92@gmail.com>  Thu, 03 Apr 2014 19:42:38 +0530
